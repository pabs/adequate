#!/bin/sh
set -e -u -x

export PATH=/usr/sbin:/usr/bin:/sbin:/bin

mount_stuff()
{
    mount -n -t proc proc /proc
    mount -n -t sysfs sysfs /sys
    mkdir -p /dev/pts
    mount -n -t devpts devpts /dev/pts
    mount -n -o bind /usr/lib/uml/modules /lib/modules
    mount -n -t tmpfs tmpfs /tmp -o size=1G
}

if [ "$1" != '--cow' ]
then
    export testdir="$(printf '%s' "$1" | tr _ = | base64 -d)"
    export testfile="$(printf '%s' "$2" | tr _ = | base64 -d)"
    mount_stuff
    mkdir -m 0700 /tmp/dev
    mknod /tmp/dev/ubda b 98 0
    mkswap -f /tmp/dev/ubda
    swapon /tmp/dev/ubda
    mkdir -m 0700 /tmp/ro /tmp/rw /tmp/aufs
    mount -n -t hostfs hostfs /tmp/ro -o /,ro
    mount -n -t aufs aufs /tmp/aufs -o noatime,noxino,dirs=/tmp/rw:/tmp/ro=ro
    exec chroot /tmp/aufs "$0" --cow
else
    mount_stuff
    rm -vf \
        /var/lib/adequate/* \
        /var/lib/dpkg/lock \
        /var/lib/dpkg/triggers/Lock \
        /var/lib/apt/listchanges.db \
        /var/log/apt/term.log \
        /var/log/apt/history.log \
        /var/log/dpkg.log \
        /var/cache/apt/archives/lock \
        /var/cache/debconf/*.dat
    cd "$testdir"
    ./run-tests "$testfile"
    halt -f
fi

# vim:ts=4 sts=4 sw=4 et
