[x] bin-or-sbin-binary-requires-usr-lib-library
[ ] broken-binfmt-detector
[ ] broken-binfmt-interpreter
[x] broken-symlink
[2] incompatible-licenses
[ ] ldd-failure
[x] library-not-found
[x] missing-alternative
[x] missing-copyright-file
[x] missing-pkgconfig-dependency
[x] missing-symbol-version-information
[x] obsolete-conffile
[x] program-name-collision
[x] py-file-not-bytecompiled
[x] pyshared-file-not-bytecompiled
[x] symbol-size-mismatch
[x] undefined-symbol
